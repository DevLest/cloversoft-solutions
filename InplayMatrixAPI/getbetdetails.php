<?php
    header("Content-type: text/html; charset=utf-8");
    $info = $_POST;

    $memberCode = $info['account'];
    $sportsId = $info['sportsId'];
    $betStatus = $info['betStatus'];
    // $memberCode = "test01";
    // $sportsId = 1;
    // $betStatus = 1;

    $date = date("Y-m-d h:m:s A");
    // $startTime = date("Y-m-d h:m:s A", strtotime($date) - (10 * 60) );
    $startTime = date("Y-m-d h:m:s A", (strtotime("-30 days")));

    $postData = [
        'timeStamp' => generate_imsb_time_stamp(return_timestamp(),'cae2e1ed7185b841'),
        'memberCode' => $memberCode,
        'currencyCode' => "RMB",
        'sportsId' => $sportsId,
        'dateFilterType' => "1",
        'startDateTime' => $startTime,
        'endDateTime' => $date,
        'betStatus' => $betStatus,
        'languageCode' => "CHS"
    ];

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://u2d668.sfsbws.imapi.net/api/getBetDetails",
        CURLOPT_FOLLOWLOCATION => 0,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_BINARYTRANSFER => true,
        CURLOPT_TIMEOUT => 3,
        CURLOPT_POST => 1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($postData),
        CURLOPT_HTTPHEADER => array(
            'Content-Type:application/json; charset=utf-8'
        ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);

    print_r(json_decode($response));

    function generate_imsb_time_stamp($data, $secret_key)
    {
        $secret_key = md5(utf8_encode($secret_key), true);
        $result = openssl_encrypt(
            $data,
            "aes-128-ecb",
            $secret_key,
            $options = OPENSSL_RAW_DATA
        );
        return base64_encode($result);
    }
    function return_timestamp()
    {
        date_default_timezone_set("GMT");
        $date = date("D, d M Y H:i:s") . " GMT";
        return $date;
    }

    function formatted_date($date)
    {
        date_default_timezone_set("Etc/GMT+4");
        $date = date_create($date);

        return date_format($date, "c");
    }