<?php

namespace PaymentMerchant;

class PaymentMerchant 
{
    protected static $publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDWvB9lIeK0c43Hz9OaiLBbvrQLINrKoJDz/mX7RaXa3QLu1OyC8ewHphMp4/NjfJgBfhld45Za/0J41ykmY5BxuF4KZvrvgmTQTXBi638oLONGW51Tup5slCLvaFPIj28hGQOpFwf/68dKHTi5VxJ7UdqPJ6VwWWq09fYg4FhVfwIDAQAB";
    protected static $privateKey = "MIICXAIBAAKBgQDWvB9lIeK0c43Hz9OaiLBbvrQLINrKoJDz/mX7RaXa3QLu1OyC8ewHphMp4/NjfJgBfhld45Za/0J41ykmY5BxuF4KZvrvgmTQTXBi638oLONGW51Tup5slCLvaFPIj28hGQOpFwf/68dKHTi5VxJ7UdqPJ6VwWWq09fYg4FhVfwIDAQABAoGAJEBuLGykEULQ1MLJPnWFEHgJfTd2uG6TCh4QqXMApQpazGc15oVhC4mGArRxCoKmRto6/hbF82bUmgH4+HomkDztTq28ecGRKG0BhwZx/UwHgyuwWNppEs+RDa0u+EJMRC6Oe/9E5hB1233G6BC/cI3YIlVzOMhCygeGfTyUr8ECQQD7ONwP9iYZd/d75H3+yg1CYSVFsQqDkr//dk8QHZFwOjSND95iJ65zfYkHfronSjRLwsRfnxk5iV/CsdKnDmtzAkEA2tGdhHtD/puKMQPvGyUxLKohYbwNu4k9UWRQa4pP5YjPykVdLSx1QZdoJoP/AnFu4loONyTsbG5UaAJ7sERCxQJAVuKhaXvOTXyqQVUTJm2nDFAg3euPIVY9xRKb4Yz4YgIfbrIQTY4/zX2bSNZRd4k4cQNZe2DBDp8uWV2650AbdQJAbjWFEGTFKkdMosNIenORAN3vUjpU8E9/Aq9zkbZn50Ow5D9rXoU1yYNogX2ZwrE/F+IjpsKCEVrASGJ3yZ+XwQJBAOpDS5Bp7EpoEcHoiSAMCxCnSzIa3Tnj00CiwtmgFop9V3P0GLKy3/uP3gapVUocK9mMxZXzQ1WgHzB259s4B3s=";
    
    public function __construct(){}

    /**
     * Fetching privateKey
     * 
     * @return string
     */
    public static function getPrivateKey()
    {
        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap(self::$privateKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";
        return $privateKey;
    }
 
    /**
     * Fetching publicKey
     * 
     * @return string
     */
    public static function getPublicKey()
    {
        $publicKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap(self::$publicKey, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";
        return $publicKey;
    }
 
    /**
     * Building Signature for api request
     * 
     * @param array
     * @return string/boolean
     */
    public static function createSign($data)
    {
        if (!is_string($data)) {
            return false;
        }

        return openssl_sign( $data, $sign, self::getPrivateKey(), OPENSSL_ALGO_SHA256 ) ? base64_encode($sign) : false;
    }
 
    /**
     * Verifying Signature for api request
     * 
     * @param array
     * @param boolean
     */
    public static function verifySign($data, $sign)
    {
        if (!is_string($sign) || !is_string($sign)) {
            return false;
        }
        return (bool) openssl_verify( $data, base64_decode($sign), self::getPublicKey(), OPENSSL_ALGO_SHA256 );
    }

    /**
     * Calls the API and process curl requests
     * 
     * @param array
     * @param string
     */
    public static function callAPI($data, $api = 'https://gateway.uyp869.com/gateway/build')
    {
        ksort($data);
        $query = http_build_query($data);

        $sign = self::createSign($query);
        $verified = self::verifySign($query,$sign);

        if ($verified)
        {
            $data['sign'] = $sign;

            $curl = curl_init();
        
            curl_setopt_array($curl, array(
                CURLOPT_URL => $api,
                CURLOPT_FOLLOWLOCATION => 0,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_BINARYTRANSFER => true,
                CURLOPT_TIMEOUT => 3,
                CURLOPT_POST => 1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json; charset=utf-8'
                ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            
            return json_decode( $response );
        }
        else
        {
            return "Signature not verified";
        }
    }

    /**
     * Converts Post inputs as array and change status codes to descriptive text
     * 
     * @param post_inputs
     * @return boolean/array
     */
    public static function getCallback($content)
    {
        $decoded = json_decode($content, true);

        if(! is_array($decoded) )
        {
            $return_value = [
                "merchant_id" => $decoded->data->merchant_id, //$decoded->data['merchant_id'];
                "order_id" => $decoded->data->order_id, //$decoded->data['order_id'];
                "pay_status" => $decoded->data->pay_status, //$decoded->data['pay_status'];
                "paid_money" => $decoded->data->paid_money, //$decoded->data['paid_money'];
                "pay_time" => $decoded->data->pay_time, //$decoded->data['pay_time'];
            ];

            ksort($return_value);

            $verified = self::verifySign( http_build_query($return_value), $decoded->data->sign );
            if ( $verified ) $return_value['sign'] = $decoded->data->sign;

            return $return_value;
        }
        else
        {
            return false;
        }
    }
    
}

//Modifications starts here
$data_array =  [
    "amount" => "300.00",
    "merchant_id" => "948810059",
    "method" => "copybank",
    "notify_url" => "google.com",
    "return_url" => "google.com",
    "order_no" => time(),
    "version" => "2.0",
];

$call_request = new PaymentMerchant();
$response = $call_request->callAPI($data_array);


if ( $response->errorCode !== 200 )
{
    echo "Error Code: ".$response->errorCode ."\n";
    echo $response->message ."\n";
}
else
{
    echo $response->data->payurl ."\n";
}


$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if ($contentType === "application/json")
{
    $callback = $call_request->getCallback( trim( file_get_contents( "php://input" ) ) );
}

$call_request->getCallback($data = $_POST['data']);