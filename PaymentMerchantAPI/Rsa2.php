<?php

namespace PaymentMerchant;

class Rsa2 {

    protected static $privateKey = '';
    protected static $publicKey = '';

    public function __construct($privateKey, $publicKey) {
        self::$privateKey = $privateKey;
        self::$publicKey = $publicKey;
    }

    public static function getPrivateKey() {
        return openssl_get_privatekey(self::$privateKey);
    }

    public static function getPublicKey() {
        return openssl_get_publickey(self::$publicKey);
    }

    public static function createSign($data = '') {
        if (!is_string($data)) {
            return false;
        }
        return openssl_sign($data, $sign, self::getPrivateKey(), OPENSSL_ALGO_SHA256) ? base64_encode($sign) : false;
    }

    public static function verifySign($data = '', $sign = '') {
        if (!is_string($sign) || !is_string($sign)) {
            return false;
        }
        return (bool) openssl_verify( $data, base64_decode($sign), self::getPublicKey(), OPENSSL_ALGO_SHA256 );
    }

    public static function callAPI($data, $api = 'https://gateway.uyp869.com/gateway/build')
    {
        ksort($data);
        $query = http_build_query($data);

        $sign = self::createSign($query);
        $verified = self::verifySign($query,$sign);

        if ($verified)
        {
            $data['sign'] = $sign;

            $curl = curl_init();
        
            curl_setopt_array($curl, array(
              CURLOPT_URL => $api,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => json_encode($data),
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json; charset=utf-8'
              ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            
            return $response;
        }
        else
        {
            return "Signature not verified";
        }
    }
}
