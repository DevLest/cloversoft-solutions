<?php
    require_once('PaymentMerchant.php');

    use PaymentMerchant\PaymentMerchant;

    //request body data in submiting payment
    $data_array =  [
        "amount" => "300.00",
        "merchant_id" => "948810059",
        "method" => "copybank",
        "notify_url" => "google.com",
        "return_url" => "google.com",
        "order_no" => "2021122333444455563", //change this order_no when testing.
        "version" => "2.0",
    ];
    
    $call_request = new PaymentMerchant();
    $response = $call_request->callAPI($data_array);

    //can get the link from $response->data->payurl
    if ( $response->errorCode !== 200 )
    {
        echo "Error Code: ".$response->errorCode ."\n";
        echo $response->message ."\n";
    }
    else
    {
        echo $response->data->payurl ."\n";
    }
    var_export( $response );


    /**
    *fetching callbacks from notify_url
    * 
    */
    $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

    //checks if request is json file
    if ($contentType === "application/json")
    {
        $callback = $call_request->getCallback( trim( file_get_contents( "php://input" ) ) );
    }
