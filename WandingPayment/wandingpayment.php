<?php
    header("Content-type: text/html; charset=utf-8");
    $info = $_POST;

    $billno = $info['billno'];
    $amount = $info['amount'];
    // $payername = $info['payername'];

    $notifyreturn_url = $info['return_url'];

    $postData = [
        'mer_trade_no' => $billno,
        'partner_id' => '4043333020965589',
        'amount' => $amount,
        'goods_name' => "testing",
        'client_ip' => "27.18.198.204",
        'charset' => "UTF-8",
        'timestamp' => date('Y-m-d H:m:s', strtotime('+9 hours', strtotime(date('Y-m-d H:m:s')))),
        'notify_url' => $notifyreturn_url,
        'pay_type' => "ALI_HF_HZY",
        'random_type' => "2",
        'sign_type' => "MD5",
    ];

    ksort($postData);
    $postData['key'] = "93fcdf4684ea41b08e31af664e28575c";

    $md5str = "";
    foreach ($postData as $key => $val) {
        $md5str .= $key . $val;
    }

    $postData['sign'] = md5( $md5str );

    unset($postData['key']);
    ksort($postData);

    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => "http://wandingmallvip.com:6300/wandingpay/pay/createPayOrder",
        CURLOPT_FOLLOWLOCATION => 0,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_BINARYTRANSFER => true,
        CURLOPT_TIMEOUT => 3,
        CURLOPT_POST => 1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => http_build_query($postData),
        CURLOPT_HTTPHEADER => [
            'Content-Type: application/x-www-form-urlencoded',
        ],
    ]);
    
    $response = curl_exec($curl);
    
    if($response === false)
    {
        print_r('Curl error: ' . curl_error($curl));
    }

    curl_close($curl);

    // $url = json_decode($response)->result->payurl;

    // header("location:$url");exit;

    print_r(json_decode($response));