<?php
    header("Content-type: text/html; charset=utf-8");
    $info = $_POST;

    // $billno = $info['billno'];
    // $amount = $info['amount'];
    // $notifyreturn_url = $info['return_url'];
    $billno = "TestOrder0002";
    $amount = 100;
    $notifyreturn_url = "http://callback.test";

    $postData = [
        'Key' => 'F6A6F52B58944EEF421308B92868562E',
        'MerchantId' => "E4Q9OIHDT0GKFNiv7s0aQ",
        'OrderId' => $billno,
        'Amount' => number_format($amount,2),
        'NotifyURL' => $notifyreturn_url,
        'Timestamp' => strtotime( date('m/d/Y') ),
        'BankName' => "李宝柱",
        'CardHolder' => "李宝柱",
        'CardNumber' => 6177,
        'IssuingBank' => "中国农业银行"
    ];

    ksort($postData);
    $postData['Sign'] = md5( urldecode( http_build_query( $postData ) ));
    unset($postData['Key']);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://www.asdsadg24324f.com/Api/Transaction/ApOrder",
        CURLOPT_FOLLOWLOCATION => 0,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_BINARYTRANSFER => true,
        CURLOPT_TIMEOUT => 3,
        CURLOPT_POST => 1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => http_build_query($postData),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
        ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);

    print_r($response);