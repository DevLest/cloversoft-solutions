<?php
    header("Content-type: text/html; charset=utf-8");
    $info = $_POST;

    $billno = $info['billno'];
    $amount = floatval($info['amount']) * 100;    
    $payername = $info['payername'];    

    $notifyreturn_url = $info['return_url'];

    $postData = [
        'amount' => $amount,
        'clientip' => "27.18.198.204",
        'currency' => "CNY",
        'mhtorderno' => $billno,
        'mhtuserid' => "Ubet",
        'notifyurl' => $notifyreturn_url,
        'opmhtid' => "Ubet",
        'payername' => $payername,
        'paytype' => "bank",
        'random' => date('Y-m-d_H_i_s'),
    ];

    ksort($postData);
    $postData['sign'] = hash_hmac("sha384", urldecode(http_build_query( $postData )), "LS8ITpkrMihE3sypCjpMrN8x2vUdauXUxwQx2n2PyT6y9rna0P9fvHzl1TEMRPwj");

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://gateway.tgapg.com:9909/api/v2/pay/placeOrder",
        CURLOPT_FOLLOWLOCATION => 0,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_BINARYTRANSFER => true,
        CURLOPT_TIMEOUT => 3,
        CURLOPT_POST => 1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => http_build_query($postData),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
        ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);

    $url = json_decode($response)->result->payurl;

    header("location:$url");exit;

    print_r(json_decode($response)->rtCode);