<?php
    header("Content-type: text/html; charset=utf-8");
    $info = $_POST;

    $billno = (isset($info['billno'])) ? $info['billno'] : "";
    $amount = (isset($info['amount'])) ? floatval($info['amount']) * 100 : "";;

    $notifyreturn_url = (isset($info['return_url'])) ? $info['return_url'] : "";
    $payername = (isset($info['payername'])) ? $info['payername'] : "";

	if ( $payername != "" ) {
		$postData = [
			'amount' => $amount,
			'clientip' => "27.18.198.204",
			'currency' => "CNY",
			'mhtorderno' => $billno,
			'mhtuserid' => "Ubet",
			'notifyurl' => $notifyreturn_url,
			'opmhtid' => "Ubet",
			'payername' => $payername,
			'paytype' => "bank",
			'random' => date('Y-m-d_H_i_s'),
		];

		ksort($postData);
		$postData['sign'] = hash_hmac("sha384", urldecode(http_build_query( $postData )), "LS8ITpkrMihE3sypCjpMrN8x2vUdauXUxwQx2n2PyT6y9rna0P9fvHzl1TEMRPwj");

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://gateway.tgapg.com:9909/api/v2/pay/placeOrder",
			CURLOPT_FOLLOWLOCATION => 0,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_BINARYTRANSFER => true,
			CURLOPT_TIMEOUT => 3,
			CURLOPT_POST => 1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => http_build_query($postData),
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));
		
		$response = curl_exec($curl);
		
		curl_close($curl);

		$url = json_decode($response)->result->payurl;

		header("location:$url");
		exit;
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<style>
			html,
			body {
				height: 100%;
				width: 100%;
				margin: 0 auto;
			}
			body {
				background-color: #108ee9;
				text-align: center;
				width: 100%;
				height: 100%;
				box-sizing: border-box;
			}
			.hidden {
				display: none;
			}
			.header {
				width: 100%;
				height: 36px;
				background-color: #fff;
				display: flex;
				align-items: center;
				padding: 0 8px;
				box-sizing: border-box;
			}
			.header > .icon {
				width: 36px;
				height: 36px;
				display: flex;
				justify-content: center;
				align-items: center;
			}
			.header > .title-main {
				line-height: 36px;
				flex: 1;
				text-align: center;
				padding-right: 36px;
			}
			.page {
				display: flex; /*Flex布局*/
				display: -webkit-flex; /* Safari */
				align-items: center; /*指定垂直居中*/
				width: 100%;
				height: 100%;
			}
			.panel {
				background-color: #fff;
				width: 80%;
				margin: 0 auto;
				max-width: 400px;
				padding: 10px;
			}
			.alert-box {
				position: absolute;
				margin: auto;
				top: 0px;
				left: 0px;
				right: 0px;
				bottom: 0;
				display: flex;
				background-color: #fff;
				border: 1px solid #787878;
				width: 300px;
				height: 200px;
				box-sizing: border-box;
				border-radius: 10px;
				flex-direction: column;
				overflow: hidden;
				align-items: center;
			}
			.alert-box > .title {
				line-height: 50px;
				font-size: 20px;
			}
			.alert-box > input {
				width: 250px;
				line-height: 40px;
				border-radius: 6px;
				padding: 0;
				border-style: none;
				border: 1px solid #7878;
				text-align: center;
				font-size: 16px;
			}
			.alert-box > .ackBut {
				margin-top: auto;
				line-height: 50px;
				width: 100%;
				background-color: #108ee9;
				letter-spacing: 5px;
				color: #fff;
				cursor: pointer;
			}
			.tooltip {
				font-size: 1.5rem;
				color: red;
				margin-top: 40px;
			}
			.pay_logos .logo {
				width: 30%;
				display: inline-block;
			}
			.pay_logos .logo img {
				height: 50px;
				vertical-align: middle;
			}
			.card {
				text-align: left;
				color: #333;
				border: 1px solid #108ee9;
				margin: 5px;
				font-size: 16px;
				background-color: aliceblue;
				border-radius: 5px;
			}
			.card .item {
				position: relative;
				padding: 10px 20px;
			}
			.card .item .btn {
				position: absolute;
				right: 10px;
				top: 8px;
				padding: 2px 10px;
				border: 0;
				border-bottom-color: currentcolor;
				border-bottom-style: none;
				border-bottom-width: 0px;
				cursor: pointer;
				margin-left: -4px;
				vertical-align: top;
				color: #fff;
				font-family: Arial;
				outline: 0;
				background: #38f;
				box-sizing: border-box;
				border-radius: 2px;
				border-bottom: 1px solid #2d78f4;
			}
			#timer {
				font-size: 1.1rem;
				padding-top: 5px;
				color: red;
			}
			#timer #seconds {
				font-size: 1.5rem;
			}

			#qrcode img {
				display: inline-block !important;
			}
			.QRcode {
				width: 200px;
				height: 200px;
				margin: 0 auto;
				display: block;
			}

			@media screen and (max-width: 550px) {
				.panel {
				background-color: #fff;
				width: 90%;
				margin: 0 auto;
				}
				.tooltip {
				font-size: 1.25rem;
				}
			}
		</style>
	</head>
	<body>
		<header class="header">
			<div class="icon" onclick="window.history.back(-1)">
				<svg
				t="1597647458800"
				style="height: 20px; width: 20px"
				viewBox="0 0 1024 1024"
				>
				<path
					d="M670.6 861.6c-7.6 0-15.1-2.9-20.9-8.6L329.4 532.7c-11.6-11.5-11.6-30.3 0-41.8l320.2-320.2c5.8-5.8 13.3-8.6 20.9-8.6 7.5 0 15.1 2.9 20.9 8.6 11.5 11.5 11.5 30.2 0 41.8L392 511.8l299.4 299.4c11.5 11.6 11.5 30.3 0 41.8-5.7 5.7-13.3 8.6-20.8 8.6z"
					p-id="1361"
				></path>
				</svg>
			</div>
		</header>
		<div class="page">
			<div class="panel" style="filter: blur(10px)">
				<div class="pay_logos">
					<div class="logo">
					</div>
				</div>
			<div style="padding: 5px 15px;text-align: left;color: #666;font-size: 12px;">
				点击"复制"按钮，复制信息后，可以通过支付宝--“转账到银行卡”，向下面银行卡转账
			</div>
			<div class="card">
				<div class="item">银行：请输入付款姓名</div>
				<div class="item">
					<span id="card_account">姓名：请输入付款姓名</span>
					<div id="btn_card_account" data-clipboard-text="请输入付款姓名" class="btn" >
						复制
					</div>
				</div>
				<div class="item">
					<span id="card_no">卡号：请输入付款姓名</span>
					<div id="btn_card_no" data-clipboard-text="请输入付款姓名" class="btn" >
						复制
					</div>
				</div>
				<div class="item">
					金额：请输入付款姓名
					<div id="btn_amount" data-clipboard-text="请输入付款姓名" class="btn" >
						复制
					</div>
				</div>
			</div>
			<div style="padding-top: 20px">订单号：请输入付款姓名</div>
			<div id="timer">请在 <span id="seconds">03:50</span> 秒内支付</div>
			<div class="tooltip"></div>
			<ul style=" color: red; text-align: left; padding-right: 10px; font-size: 13px; margin-block-start: 10px; margin-block-end: 10px; padding-inline-start: 30px;" >
				<li>1.请复制金额转账，修改金额一定会导致无法到账！</li>
				<li>2.该卡号仅一次有效，请勿重复转账！</li>
				<li>3.请在5分钟内完成该转账，超时转账一定无法到账！</li>
			</ul>
			</div>
		    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
				<div class="alert-box" id="alert_box_switch">
					<span class="title">请输入付款人姓名</span>
					<span style="color: red; font-size: 18px; margin-bottom: 10px"> 请正确填写，否则无法到账！</span>
					<input type="text" name="payername" id="payername" placeholder="请输入付款人姓名" autocomplete="off" require/>
					<input type="text" name="billno" id="billno" value="<?php echo $_POST['billno'] ?>" hidden/>
					<input type="text" name="amount" id="amount" value="<?php echo $_POST['amount'] ?>" hidden/>
					<input type="text" name="return_url" id="return_url" value="<?php echo $_POST['return_url'] ?>" hidden/>
			        <input type="submit" class="ackBut" value="确定" />
				</div>
		    </form>
		</div>
	</body>
</html>
