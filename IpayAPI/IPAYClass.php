<?php

namespace IPAY;

class IPAYClass 
{
    protected $signKey = "LS8ITpkrMihE3sypCjpMrN8x2vUdauXUxwQx2n2PyT6y9rna0P9fvHzl1TEMRPwj";
    protected $APIGateway = "http://gateway.tgapg.com:9909/";

    public function __construct(){}

    /**
     * Creation of payment 
     * @param array
     * 
     * @return object
     */
    public function createPaymentOrder($data)
    {
        $parameters = [
            'amount' => $data['amount'],
            'clientip' => $this->getUserIpAddr(),
            'currency' => $data['currency'],
            'mhtorderno' => $data['orderNo'],
            'mhtuserid' => $data['userID'],
            'notifyurl' => $data['notifyURL'],
            'opmhtid' => "Ubet",
            'payername' => $data['customerName'],
            'payerphone' => $data['customerPhone'],
            'paytype' => $data['paymentType'],
            'random' => date('Y-m-d_H_i_s'),
            'returnurl' => $data['returnURL'],
        ];

        $parameters['sign'] = $this->createSign($parameters);
        
        return $this->callAPI($parameters, 'api/v2/pay/placeOrder' );
    }

    /**
     * Get and Verify signature to callback parameters
     * 
     * @return array
     */
    public function getPaymentCallback()
    {
        $return_value = [
            'accno' => $_POST['accno'],
            'attach' => $_POST['attach'],
            'currency' => $_POST['currency'],
            'mhtorderno' => $_POST['mhtorderno'],
            'note' => $_POST['note'],
            'paidamount' => $_POST['paidamount'],
            'paytype' => $_POST['paytype'],
            'pforderno' => $_POST['pforderno'],
            'random' => $_POST['random'],
            'status' => $_POST['status'],
        ];

        $verified = $this->verifySign( $return_value, $_POST['sign'] );

        if ( $verified )
        {
            return $return_value;
        } 
        else 
        {
            return "Error in verifying Sign";
        }
    }

    /**
     * Gets payment status information
     * 
     * @param string
     * @return object
     */
    public function transactionInfo($orderNo, $type = 1)
    {
        $parameters = [
            'mhtorderno' => $orderNo,
            'opmhtid' => "Ubet",
            'random' => date('Y-m-d_H_i_s'),
        ];

        $parameters['sign'] = $this->createSign($parameters);
        
        switch ($type) {
            case 2:
                $call = "pay/getWithdrawInfo";
                break;
            case 3:
                $call = "payout/getInfo";
                break;
            default:
                $call = "pay/getInfo";
          }

        return $this->callAPI($parameters, 'api/v2/'.$call, 'GET' );
    }

    /**
     * Gets balance payment
     * 
     * @return object
     */
    public function getBalance($type = 1)
    {
        $parameters = [
            'opmhtid' => "Ubet",
            'random' => date('Y-m-d_H_i_s'),
        ];

        $method = ($type == 1) ? "pay" : "payout";

        $parameters['sign'] = $this->createSign($parameters);

        return $this->callAPI($parameters, "api/v2/$method/getBalance", 'GET' );
    }

    /**
     * Payment withdraw
     * 
     * @param array
     * @return object
     */
    public function paymentWithdraw($data)
    {
        $parameters = [
            'accname' => $data['accountName'],
            'accno' => $data['accountNo'],
            'acctype' => $data['accountType'],
            'amount' => $data['amount'],
            'currency' => $data['currency'],
            'mhtorderno' => $data['orderNo'],
            'notifyurl' => $data['notifyURL'],
            'opmhtid' => "Ubet",
            'random' => date('Y-m-d_H_i_s'),
        ];

        $parameters['sign'] = $this->createSign($parameters);
        // echo "\n".$parameters['sign']."\n";

        return $this->callAPI($parameters, 'api/v2/pay/withdraw' );
    }

    /**
     * Get and Verify signature to callback parameters
     * 
     * @return array
     */
    public function getWithdrawCallback()
    {
        $return_value = [
            'acctype' => $_POST['acctype'],
            'amount' => $_POST['amount'],
            'currency' => $_POST['currency'],
            'fee' => $_POST['fee'],
            'mhtorderno' => $_POST['mhtorderno'],
            'pforderno' => $_POST['pforderno'],
            'random' => $_POST['random'],
            'resultcode' => $_POST['resultcode'],
            'withdrawtime' => $_POST['withdrawtime']
        ];

        $verified = $this->verifySign( $return_value, $_POST['sign'] );

        if ( $verified )
        {
            return $return_value;
        } 
        else 
        {
            return "Error in verifying Sign";
        }
    }

    /**
     * Creation of Payout Order 
     * @param array
     * 
     * @return object
     */
    public function createPayout($data)
    {
        $parameters = [
            'acccityname' => $data['bankCity'],
            'accname' => $data['accountName'],
            'accno' => $data['accountNo'],
            'accprovince' => $data['bankState'],
            'acctype' => $data['accountType'],
            'amount' => $data['amount'],
            'bankcode' => $data['bankcode'],
            'currency' => $data['currency'],
            'mhtorderno' => $data['orderID'],
            'notifyurl' => $data['notifyURL'],
            'opmhtid' => "Ubet",
            'payerphone' => $data['customerPhone'],
            'random' => date('Y-m-d_H_i_s'),
            'remark' => $data['remark']
        ];

        $parameters['sign'] = $this->createSign($parameters);

        return $this->callAPI($parameters, 'api/v2/payout/placeOrder' );
    }

    /**
     * Gets bank codes
     * 
     * @param string
     * @return object
     */
    public function getBanckCodes($currency)
    {
        $parameters = [
            'cur' => $currency,
        ];

        return $this->callAPI($parameters, 'api/v2/payout/getBankList', 'GET' );
    }

    /**
     * Merchant Will verify the payout request
     * 
     */
    public function payoutVerify()
    {
        $data = $_GET['opmhtid'].$_GET['mhtorderno'].$_GET['accno'].$_GET['amount'].$this->signKey;

        if ( $_GET['data'] == $this->createSign($parameters, "md5") )
        {
            return "ok";
        } 
        else 
        {
            return "Error in verifying DATA";
        }
    }

    /**
     * Get and Verify signature to callback parameters
     * 
     * @return array
     */
    public function getPayoutCallback()
    {
        $return_value = [
            'afterbalance' => $_POST['afterbalance'],
            'amount' => $_POST['amount'],
            'currency' => $_POST['currency'],
            'mhtorderno' => $_POST['mhtorderno'],
            'note' => $_POST['note'],
            'payouttime' => $_POST['payouttime'],
            'pforderno' => $_POST['pforderno'],
            'random' => $_POST['random'],
            'resultcode' => $_POST['resultcode']
        ];

        $verified = $this->verifySign( $return_value, $_POST['sign'] );

        if ( $verified )
        {
            return $return_value;
        } 
        else 
        {
            return "Error in verifying Sign";
        }
    }

    /**
     * Building Signature for api request
     * 
     * @param array
     * @return string
     */
    private function createSign($data, $method = "sha384")
    {
        ksort($data);

        return  hash_hmac($method, urldecode(http_build_query( $data )), $this->signKey);
    }

    /**
     * Verifying Signature for api request
     * 
     * @param array
     * @param boolean
     */
    private function verifySign($data, $sign)
    {
        if (!is_string($sign) || !is_string($sign)) {
            return false;
        }
        
        return (bool) hash_equals( $this->createSign( $data ), $sign );
    }

    /**
     * Calls the API and process curl requests
     * 
     * @param array
     * @param object
     */
    private function callAPI($data, $url, $requestMethod = "POST")
    {
        ksort($data);
        
        $curl = curl_init();
        $requestURL = $this->APIGateway.$url;
        $requestData = http_build_query($data);

        curl_setopt_array($curl, array(
            CURLOPT_FOLLOWLOCATION => 0,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_TIMEOUT => 3,
            CURLOPT_POST => 1,
            CURLOPT_CUSTOMREQUEST => $requestMethod,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        if ( $requestMethod == "POST" ) 
        {
            curl_setopt($curl, CURLOPT_URL, $requestURL);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $requestData);
        }
        else
        {
            curl_setopt($curl, CURLOPT_URL, $requestURL."?".$requestData);
        }
        
        $response = curl_exec($curl);
        
        curl_close($curl);

        return json_decode( $response );
    }

    /**
     * Gets real IP address of a user
     * 
     * @return string
     */
    private function getUserIpAddr()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = '127.0.0.1';
        return $ipaddress;
    }
}