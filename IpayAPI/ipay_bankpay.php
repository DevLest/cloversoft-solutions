<?php
    header("Content-type: text/html; charset=utf-8");
    
    include_once ("../u2dcorefornew.class.php");
    
    $core = new corefornew("mysqli");
    error_reporting(0);

    $sign = $_POST['sign'];
    $param = $_POST;

    unset($param['sign']);

    if ( hash_equals( hash_hmac("sha384", urldecode( http_build_query( $param ) ), "LS8ITpkrMihE3sypCjpMrN8x2vUdauXUxwQx2n2PyT6y9rna0P9fvHzl1TEMRPwj" ), $sign ) && $param['status'] )
    {
        $order_no = $param['mhtorderno'];
        $trade_no = $param['pforderno'];
        $amount = round( floatval( $param['paidamount']) / 100 );
        
        $arr = [
            "ratio"=>0
        ];

        if( $core->onlinepay_sure($order_no,$trade_no,$amount,$arr) )
        {
            error_log(date('m-d H:i:s')."success#".$order_no."#".$trade_no."#".$amount."#\r\n", 3,"yadong_bankpay.log");
        }
        else
        {
            error_log(date('m-d H:i:s')."fail#".$order_no."#".$trade_no."#".$amount."#\r\n", 3,"yadong_bankpay.log");
            exit;
        }

        echo 'OK';
    }
    else
    {
        error_log(date('m-d H:i:s')."fail222#".json_encode($data)."###\r\n", 3,"yadong_bankpay.log");
        exit;
    }
