<?php
    include_once('IPAYClass.php');

    use IPAY\IPAYClass;

    $ipay = new IPAYClass();

    $orderNo = "20210427TestRequest4";
    $userID = "test01";
    $amount = 20000; //amount in cents

    /**
     * Creation of payment
     * 
     * @param array
     * 
     * @return object
     */
    $payment = [
        'amount' => $amount,
        'currency' => 'CNY', //Fiat Currency: CNY,EUR,INR,JPY,USD | Crypto Currency: USDT
        'orderNo' => $orderNo,
        'userID' => $userID,
        'customerName' => 'testingplayer',
        'customerPhone' => '35682445',
        'paymentType' => 'bank', // alipay, alipay_alipay, alipay_bank, bank, bank_float, credit_card, crypto, crypto_payasyougo, inbank, inbank_upi, iwallet, vnbank
        'notifyURL' => "http://callback.com", //Callback endpoint when the payment succeeds.     
        'returnURL' => "http://return.com/returnStatus", //Redirect URL after payment is successful
    ];

    var_export($ipay->createPaymentOrder($payment));


    /**
    *Fetching callbacks from notifyURL
    * 
    */
    if (!empty($_POST))
    {
        $callback = $ipay->getPaymentCallback();

        var_export($callback);
    }


    /**
     * Check Payment Status
     * 
     */
    var_export($ipay->transactionInfo($orderNo, 1)); // 1 = payment, 2 = withdraw, 3 = payout


    /**
     * Get Balance Payment
     */
    var_export($ipay->getBalance(1));

    
    /**
     * Create payment withdraw order
     */
    var_export($ipay->paymentWithdraw());


    /**
     * Payment Withdraw
     */
    $withdraw = [
        // 'accountName' => "testBank", //not available
        // 'accountNo' => 3255267894584156, //not available
        'accountType' => "crypto", // crypto, bank_card (unavailable for now)
        'amount' => $amount,
        'currency' => 'USDT', //Fiat Currency: CNY,INR | Crypto Currency: USDT
        'orderNo' => $orderNo,
        'notifyURL' => "http://callbackWithdraw.com", //Callback endpoint when the withdraw succeeds.     
    ];

    var_export($ipay->paymentWithdraw($withdraw));

    
    /**
    *Fetching callbacks from notifyURL
    * 
    */
    if (!empty($_POST))
    {
        $callback = $ipay->getWithdrawCallback();

        var_export($callback);
    }

    
    /**
     * Check Withdraw Status
     * 
     */
    var_export($ipay->transactionInfo($orderNo, 2)); // 1 = payment, 2 = withdraw, 3 = payout

    
    /**
     * Payout
     */
    $payout = [ 
        'bankCity' => "Wakanda",
        'accountName' => "test account",
        'accountNo' => "3255267894584156",
        'bankState' => "Utah",
        'accountType' => "bank_card", // bank_card, crypto, iwallet
        'amount' => $amount,
        'bankcode' => "ZYCBANK", // call $ipay->getBanckCodes() tot get available banks
        'currency' => "CNY", //Fiat Currency: CNY,EUR,INR,JPY,USD | Crypto Currency: USDT
        'orderID' => $orderNo,
        'notifyURL' => "http://callbackPayout.com",
        'customerPhone' => "35682445",
        'remark' => "This is test",
    ];
    
    var_export($ipay->createPayout($payout));


    /**
     * Get Bank Codes Available
     */
    var_export($ipay->getBanckCodes("CNY"));

    
    /**
     * Verify Payout call back
     */
    
    if (!empty($_GET))
    {
        $verifyPayout = $ipay->payoutVerify();

        echo $verifyPayout;
    }

    
    /**
    *Fetching callbacks from notifyURL
    * 
    */
    if (!empty($_POST))
    {
        $callback = $ipay->getWithdrawCallback();

        var_export($callback);
    }

    
    /**
     * Check Payout Status
     * 
     */
    var_export($ipay->transactionInfo($orderNo, 3)); // 1 = payment, 2 = withdraw, 3 = payout

    
    /**
     * Get Balance Payment
     */
    var_export($ipay->getBalance(2));