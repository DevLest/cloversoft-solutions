<?php

namespace TCG;

class TCGClass 
{
    protected   $merchantCode = 'atpking',
                $DESkey = 'hMrLh2A3',
                $signKey = 'vvbXY4Ty9a5VmecE',
                $currency;

    public function __construct($currency){
        $this->currency = $currency;
    }

    /**
     * Creation of User/Player
     * 
     * @param username
     * @param password
     * @return object
     */
    public function createUser( $username, $password )
    {
        $parameters = [
            'username'  => $username,
            'password'  => $password,
            'currency'  => $this->currency,
            'method'    => 'cm',
        ];

        return $this->sendRequest( $parameters );
    }

    /**
     * Reset/Change password of User/Player
     * 
     * @param username
     * @param password
     * @return object
     */

    public function changePassword( $username, $password )
    {
        $parameters = [
            'username'  => $username,
            'password'  => $password,
            'currency'  => $this->currency,
            'method'    => 'up',
        ];

        return $this->sendRequest( $parameters );
    }

    /**
     * Get Available balance
     * 
     * @param username
     * @param int
     * @return object
     */
    public function getBalance( $username, $productType )
    {
        $parameters = [
            'username'  => $username,
            'product_type'  => $productType,
            'method'    => 'gb',
        ];

        return $this->sendRequest( $parameters );
    }

    /**
     * Fund Out/Transfer 
     * 
     * @param username
     * @param int | product type
     * @param int | fund type | Optional
     * @param float/double | amount 
     * @param string | reference number
     * 
     * @return object
     */
    public function fundTransfer( $username, $productType, $amount, $referenceNo, $fundType = 2 )
    {
        $parameters = [
            'username'  => $username,
            'method'    => 'ft',
            'product_type'  => $productType,
            'fund_type'  => $fundType,
            'amount'  => $amount,
            'reference_no'  => $referenceNo,
        ];

        return $this->sendRequest($parameters);
    }
    
    /**
     * Check transfer status
     * 
     * @param int | product type
     * @param sting | reference number
     * 
     * @return object
     */
    public function checkTransfer($productType, $referenceNo)
    {   
        $parameters = [
            'method'    => 'cs',
            'product_type'  => $productType,
            'ref_no'  => $referenceNo,
        ];
        
        return $this->sendRequest($parameters);
    }

    /**
     * Lunch game interface
     * 
     * @param string | username
     * @param int | product type
     * @param string | game mode | 1 = Real, 0 = Trail
     * @param string | game code
     * @param string | platform
     * 
     * @return object
     */
    public function lunchGame($username, $productType, $gameMode, $gameCode, $platform)
    {
        $parameters = [
            'username'    => $username,
            'method'    => 'lg',
            'product_type'  => $productType,
            'game_mode'  => $gameMode,
            'game_code'  => $gameCode,
            'platform'  => $platform,
        ];
        
		return $this->sendRequest($parameters);
    }

    /**
     * Get game list
     * 
     * @param int | product type
     * @param string | platform 
     * @param string | client type
     * @param string | game type | RNG, LIVE, PVP, FISH, SPORTS,ELOTT
     * @param int | page | default 1
     * @param int | page size
     * 
     * @return object 
     */
	public function getGameList($productType, $platform, $clientType, $gameType, $page, $pageSize)
    {
        $parameters = [
            'method'    => 'tgl',
            'product_type'  => $productType,
            'platform'  => $platform,
            'client_type'  => $clientType,
            'game_type'  => $gameType,
            'page'  => $page,
            'page_size'  => $pageSize,
        ];

		return $this->sendRequest($parameters);
	}

    /**
     * API Request call
     * 
     * @param array
     * @return message
     */
    private function sendRequest($parameters)
    {
        try 
        {
            $params = $this->encryptText( json_encode($parameters), $this->DESkey );
            $signature = hash( 'sha256', $params.$this->signKey );
    
            $data = [
                'merchant_code' => $this->merchantCode,
                'params'        => $params,
                'sign'          => $signature
            ];
    
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://www.connect6play.com/doBusiness.do",
                CURLOPT_FOLLOWLOCATION => 0,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_BINARYTRANSFER => true,
                CURLOPT_TIMEOUT => 3,
                CURLOPT_POST => 1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => http_build_query($data),
                CURLOPT_HTTPHEADER => array(
                    "Content-type: application/x-www-form-urlencoded"
                ),
            ));
    
            $response = json_decode( curl_exec($curl) );
            
            curl_close($curl);
        }
        catch (\Exception $e)
        {
            $response = $e->getMessage();
        }

        return $response;
    }

    /**
     * Encryption of parameters
     * 
     * @param string
     * @param string
     * 
     * @return string
     */
    private function encryptText( $plainText, $key )
    {
		$padded = $this->pkcs5_pad($plainText, 8);
        $encText = openssl_encrypt($padded, 'des-ecb', $key, OPENSSL_RAW_DATA, '');
		
        return base64_encode($encText);
    }

    /**
     * Decryption of parameters
     * 
     * @param string
     * @param string
     * 
     * @return string
     */
    private function decryptText( $encryptText, $key )
    {
        $cipherText = base64_decode($encryptText);

        $res = mcrypt_decrypt("des", $key, $cipherText, "ecb");

        $resUnpadded = $this->pkcs5_unpad($res);

        return $resUnpadded;
    }
    
    /**
     * PKCS5 Padding
     * 
     * @param string
     * @param int
     * 
     * @return string
     */
    private function pkcs5_pad ($text, $blocksize)
    {
        $pad = $blocksize - ( strlen( $text ) % $blocksize );

        return $text . str_repeat( chr( $pad ), $pad );
    }
	
    /**
     * PKCS5 un-padding
     * 
     * @param string
     * 
     * @return string
     */
    private function pkcs5_unpad($text)
    {
        $pad = ord( $text{ strlen( $text ) - 1 } );

        if ( $pad > strlen( $text ) ) return false;

        if ( strspn( $text, chr( $pad ), strlen( $text ) - $pad ) != $pad ) return false;

        return substr( $text, 0, -1 * $pad );
    }
}