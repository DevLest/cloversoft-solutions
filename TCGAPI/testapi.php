<?php
    include_once('TCGClass.php');

    use TCG\TCGClass;
    
    $username = "test01";
    $password = "test01";
    $productType = 62; //115, 76, 55, 121, 53, 62, 95
    $referenceNo = "202104241234567";
    $platform = 'html5';

    /**
     * Initiate Class 
     * 
     * @param currency to be used
     */
    $tcg = new TCGClass('CNY');

    /**
     * Creation/Registration of player
     * 
     * @param username
     * @param password
     * 
     * @return object|string
     */
    $userCreation = $tcg->createUser( $username, $password );

    if ( $userCreation && $userCreation->status == 22  )
    {
        echo "Please check your parameters | username should be 4 - 14 lowercase characters from a-z with numbers from 0-9 and password should be 6 - 12 characters 0-9 A-z | No special characters";
    }
    echo "\n User Creation";

    /**
     * Update user password
     * 
     * @param username
     * @param password
     * 
     * @return object|string
     */
    $newpassword = "winterS";

    $passwordChange = $tcg->changePassword( $username, $newpassword );
    
    echo "\n Change Password";
    var_export($passwordChange);

    /**
     * Get Balance
     * 
     * @param username
     * @param int
     * 
     * @return object|string
     */
    $balance = $tcg->getBalance( $username, $productType );
    
    echo "\n Check Balance";

    if ( $balance->status == 0 )
    {
        echo "\n available Balance is: ".$balance->balance;
    }
    var_export($balance);

    /**
     * Fund transfer
     * 
     * @param string | username
     * @param int | productType
     * @param float/double | amount
     * @param string | reference number
     * @param int | fund type | 1 = transfer in, 2 = transfer out | default 2
     * 
     * @return object|string 
     */
    $fundType = 1;
    $amount = 1;

    $fundTransfer = $tcg->fundTransfer($username, $productType, $amount, $referenceNo, $fundType);
    
    echo "\n Fund Transfer";
    var_export($fundTransfer);

    /**
     * Check fund transafer
     * 
     * @param int | product type
     * @param string | reference number
     * 
     * @return object
     */
    $checkTransfer = $tcg->checkTransfer($productType, $referenceNo);

    echo "\n Check Transfer";
    var_export($checkTransfer);

    /**
     * Lunch game interface
     * 
     * @param string | username
     * @param int | product type
     * @param string | game mode | 1 = Real, 0 = Trail
     */
    $gameMode = 1;
    $gameCode = 'DTQ001'; //DTQ
    // $gameCode = 'GPI200'; //GPI
    // $gameCode = 'JDB159'; //JDB
    // $gameCode = 'KM0027'; //JDB

    $lunchGame = $tcg->lunchGame($username, $productType, $gameMode, $gameCode, $platform);

    echo "\n Lunch Game";

    if ( $lunchGame->status == 0 )
    {
        echo "\n Open this URL to lunch game: ".$lunchGame->game_url;
    }
    var_export($lunchGame);

    /**
     * Get game list
     * 
     * @param int | product type
     * @param string | platform 
     * @param string | client type
     * @param string | game type | RNG, LIVE, PVP, FISH, SPORTS,ELOTT
     * @param int | page | default 1
     * @param int | page size
     */
    $clientType = "web";
    $gameType = "PVP";
    $page = 1 ;
    $pageSize = 100;

    $gameList = $tcg->getGameList($productType, $platform, $clientType, $gameType, $page, $pageSize);

    echo "\n Game List";
    var_export($gameList);